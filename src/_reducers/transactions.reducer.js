import { userConstants } from '../_constants';

export function transactions(state = [], action) {
    switch (action.type) {
        case userConstants.GETTRANSACTIONS_REQUEST:
            return [];
        case userConstants.GETTRANSACTIONS_SUCCESS:
            return action.transactions;
        default:
            return state
    }
}