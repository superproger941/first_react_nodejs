import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './_helpers';
import { App } from './App';
import {Col, Divider, Row} from "antd";

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);

/*
created_dt: unixtime — время создания (временно пусто или  B26+F28, чтобы уникально было)
tx_id — номер транзакции (временно пусто или  B26+F28, чтобы уникально было)


reward_id — номер транзакции на выручку (временно пусть или B26+B32, чтобы уникально было)

user_id — пользователь (F24)
tx_amt — сумма, если >0 то приход, если <0 то расход (F27)
reward_amt — выручку (F33, G33,... для разных клиентов)
balance_amt — баланс на старт (F28)
total_amt — баланс на конец (F35)





Вторая таблица – с циклами инвестирования:
    created_dt: unixtime — время создания/начало цикла (B26)
tx_amt — сумма инвестирования (B27)
tx_dt: unixtime — время создания (B26 временно)


net_amt — сумма проекта (B30)
tx_returned_amt — сумма возврата (B32)
*/
/*
<Divider/>
<Row>
    <Col span={8}>
        <h3>Statement</h3>
    </Col>


    <Col span={16}>
        <Row>
            <Col span={8}>
                <Row>
                    <Col span={24}>
                        <b>name</b>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        <b>wallet</b>
                    </Col>
                </Row>
            </Col>
            <Col span={8}>
                <Row>
                    <b>Acc0</b>
                </Row>
                <Row>
                    {item.user_id}
                </Row>
            </Col>
            <Col span={8}>
                <b>Acc1</b>
            </Col>
        </Row>
    </Col>
</Row>
<Row>
    <Col span={8}>
        <Row>
            <Col span={12}>
                <b>Cycle</b>
            </Col>
            <Col span={12}>
                25/09 22h
            </Col>
        </Row>
    </Col>


    <Col span={16}>
        <Row>
            <Col span={8}>
                <b>Incoming Reward</b>
            </Col>
            <Col span={8}>
                <b></b>
            </Col>
            <Col span={8}>
                <b></b>
            </Col>
        </Row>
    </Col>
</Row>
<Row>
    <Col span={8}>
        <Row>
            <Col span={12}>
                <b>Stake Tx</b>
            </Col>
            <Col span={12}>
                221,500
            </Col>
        </Row>
    </Col>
    <Col span={16}>
        <Row>
            <Col span={8}>
                <b>Incoming Tx</b>
            </Col>
            <Col span={8}>
                <b>{item.tx_amt}</b>
            </Col>
            <Col span={8}>
                <b>73,480</b>
            </Col>
        </Row>
    </Col>
</Row>
<Row>
    <Col span={8}>
        <Row>
            <Col span={12}>
            </Col>
            <Col span={12}>
            </Col>
        </Row>
    </Col>
    <Col span={16}>
        <Row>
            <Col span={8}>
                <b>Balance</b>
            </Col>
            <Col span={8}>
                <b>{item.balance_amt}</b>
            </Col>
            <Col span={8}>
                <b>73,480</b>
            </Col>
        </Row>
    </Col>
</Row>

<Row style={{
    marginTop: '20px',
}}>
    <Col span={8}>
        <Row>
            <Col span={12}>
                <b>Net stakes</b>
            </Col>
            <Col span={12}>
                {item.net_amt}
            </Col>
        </Row>
    </Col>
    <Col span={16}>
        <Row>
            <Col span={8}>
                Cycle in (staked)
            </Col>
            <Col span={8}>
                160,548.49
            </Col>
            <Col span={8}>
                60,951.51
            </Col>
        </Row>
    </Col>
</Row>

<Row>
    <Col span={8}>
        <Row>
            <Col span={12}>
            </Col>
            <Col span={12}>
            </Col>
        </Row>
    </Col>
    <Col span={16}>
        <Row>
            <Col span={8}>
                Stake next cycle
            </Col>
            <Col span={8}>
                33,000.51
            </Col>
            <Col span={8}>
                12,528.49
            </Col>
        </Row>
    </Col>
</Row>

<Row>
    <Col span={8}>
        <Row>
            <Col span={12}>
                <b>Return Tx</b>
            </Col>
            <Col span={12}>
                {item.tx_returned_amt}
            </Col>
        </Row>
    </Col>
</Row>

<Row>
    <Col span={8}>
        <Row>
            <Col span={12}>
                <b>Reward</b>
            </Col>
            <Col span={12}>
                631
            </Col>
        </Row>
    </Col>
    <Col span={16}>
        <Row>
            <Col span={8}>
                <b>Reward</b>
            </Col>
            <Col span={8}>
                <b>{item.reward_amt}</b>
            </Col>
            <Col span={8}>
                <b>173.64</b>
            </Col>
        </Row>
    </Col>
</Row>

<Row>
    <Col span={8}>
        <Row>
            <Col span={12}>
            </Col>
            <Col span={12}>
            </Col>
        </Row>
    </Col>
    <Col span={16}>
        <Row>
            <Col span={8}>
                Cycle out
            </Col>
            <Col span={8}>
                161,005.86
            </Col>
            <Col span={8}>
                61,125.14
            </Col>
        </Row>
    </Col>
</Row>

<Row>
    <Col span={8}>
        <Row>
            <Col span={12}>
            </Col>
            <Col span={12}>
            </Col>
        </Row>
    </Col>
    <Col span={16}>
        <Row>
            <Col span={8}>
                <b>Total</b>
            </Col>
            <Col span={8}>
                <b>{item.total_amt}</b>
            </Col>
            <Col span={8}>
                <b>73,654</b>
            </Col>
        </Row>
    </Col>
</Row>
<Divider/>*/